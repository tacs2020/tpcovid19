# TPcovid19 API

# Tecnologias a usar
* Springboot 2 sobre Java 11
* Gradle
* Docker

## Links 
Springboot Data Rest
https://www.baeldung.com/spring-data-rest-intro

Testing
https://developer.okta.com/blog/2019/03/28/test-java-spring-boot-junit5#test-your-secured-spring-boot-application-with-junit-5

## Instalacion
```
git clone git@gitlab.com:tacs2020/tpcovid19.git
```

## Intellij
```
importar folder api
run Covid19Application
```

## Run
```
cd api
./gradlew bootRun
```

## Testing
```
./gradlew test
```

## Docker
```
cd api
./gradlew clean bootJar
docker build --tag covid19-r1 ./ 
docker run -it --rm -p 8080:8080 --name covid19 covid19-r1
```