package edu.tacs.covid19;

import edu.tacs.covid19.telegram.TelegramBot;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Covid19Application {

	public static void main(String[] args) {
		ApiContextInitializer.init();
		SpringApplication.run(Covid19Application.class, args);
	}

	@Bean
	ModelMapper engine() {
		return new ModelMapper();
	}

}