package edu.tacs.covid19.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.tacs.covid19.domain.User;

import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<User, UUID> {
    User findByUsername(@Param("username") String name);
    User findByUsernameAndPassword(@Param("username") String name, @Param("password") String password);
}