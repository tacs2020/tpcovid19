package edu.tacs.covid19.user;

import edu.tacs.covid19.domain.SingUpForm;
import edu.tacs.covid19.domain.User;

public interface IUserService {
	User login(String username, String passwrod);

	User signup(SingUpForm user);
}
