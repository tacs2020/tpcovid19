package edu.tacs.covid19.user;

import edu.tacs.covid19.domain.SingUpForm;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import edu.tacs.covid19.domain.User;
import org.springframework.web.client.HttpClientErrorException;

@Service
public class UserService implements IUserService {
    final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    UserRepository repository;

    @Autowired
    ModelMapper modelMapper;
    
    @Override
    public User login(String username, String password) {
        User userRetrieved = repository.findByUsernameAndPassword(username, password);
        if (userRetrieved == null) {
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Usuario o contraseña invalidos");
        }
        return userRetrieved;

    }
    
    @Override
    public User signup(SingUpForm user) {
        User userRetrieved = repository.findByUsername(user.getUsername());
        if (userRetrieved != null) {
            throw new ResourceNotFoundException("User not found");
        }
        return repository.save(modelMapper.map(user, User.class));
    }
}
