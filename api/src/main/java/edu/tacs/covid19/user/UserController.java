package edu.tacs.covid19.user;

import edu.tacs.covid19.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    IUserService service;
    
    @PostMapping(value= "/login")
    public User login(LoginForm form) {
    	return service.login(form.getUsername(), form.getPassword());
    }

    @GetMapping(value= "/logout")
    public String logout() {
        return "Succesful logout";
    }
    
    @PostMapping(value= "/signup")
    public User signup(SingUpForm user) {
    	return service.signup(user);
    }

    @GetMapping(value= "/subscriptions")
    public List<Subscription> getSuscriptions(){
        return new ArrayList<>();
    }
    @GetMapping(value= "/cant-subscriptions")
    public Integer getCantidadSuscriptions(){
        return this.getSuscriptions().size();
    }
    @GetMapping(value= "/ultimo-acceso")
    public LocalDateTime getUltimoAcceso(){
        return LocalDateTime.now();
    }

    @GetMapping(value= "/paises-comun")
    public List<Country> getPaisesEnComun(Subscription subscription1, Subscription subscription2){
        return new ArrayList<>();
    }

    @GetMapping(value= "/usuarios-interesados")
    public List<User> getUsuariosInteresados(Country country){
        return new ArrayList<>();
    }

    @GetMapping(value= "/subscripciones-registradas/hoy")
    public List<Subscription> getSubscripcionesRegistradasHoy(){
        return new ArrayList<>();
    }

    @GetMapping(value= "/subscripciones-registradas/ultimos-3")
    public List<Subscription> getSubscripcionesRegistradasUltimos3Dias(){
        return new ArrayList<>();
    }

    @GetMapping(value= "/subscripciones-registradas/semana-pasada")
    public List<Subscription> getSubscripcionesRegistradasSemanaPasada(){
        return new ArrayList<>();
    }

    @GetMapping(value= "/subscripciones-registradas/ultimo-mes")
    public List<Subscription> getSubscripcionesRegistradasUltimoMes(){
        return new ArrayList<>();
    }

    @GetMapping(value= "/subscripciones-registradas/inicio-tiempos")
    public List<Subscription> getSubscripcionesRegistradasDesdeInicioDeLosTiempos(){
        return new ArrayList<>();
    }

}
