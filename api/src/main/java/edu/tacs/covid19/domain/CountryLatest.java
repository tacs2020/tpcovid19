package edu.tacs.covid19.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class CountryLatest {
    @Id
    private Long id;

    private LocalDateTime lastupdate;
    private Long confirmed;
    private Long deaths;
    private Long recovered;

}
