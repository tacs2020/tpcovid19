package edu.tacs.covid19.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class CountryTimeSerie {

    @Id @GeneratedValue
    private Long id;

    private LocalDate date;
    private Long confirmed;
    private Long deaths;
    private Long recovered;

    public CountryTimeSerie() {}

    public CountryTimeSerie(LocalDate date, Long confirmed, Long deaths, Long recovered) {
        this.date = date;
        this.confirmed = confirmed;
        this.deaths = deaths;
        this.recovered = recovered;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getConfirmed() {
        return confirmed;
    }
    public void setConfirmed(Long confirmed) {
        this.confirmed = confirmed;
    }

    public Long getDeaths() {
        return deaths;
    }
    public void setDeaths(Long deaths) {
        this.deaths = deaths;
    }

    public Long getRecovered() {
        return recovered;
    }
    public void setRecovered(Long recovered) {
        this.recovered = recovered;
    }
}
