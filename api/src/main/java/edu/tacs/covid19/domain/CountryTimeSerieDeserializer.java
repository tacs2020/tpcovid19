package edu.tacs.covid19.domain;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CountryTimeSerieDeserializer extends JsonDeserializer<List<CountryTimeSerie>> {

    @Override
    public List<CountryTimeSerie> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        final List<CountryTimeSerie> list = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yy");
        JsonNode node = jp.getCodec().readTree(jp);
        if (!node.isNull() && !node.isEmpty() && node.elements().hasNext()) {
            for (Iterator<Map.Entry<String, JsonNode>> it = node.fields(); it.hasNext(); ) {
                Map.Entry<String, JsonNode> serie = it.next();
                JsonNode values = serie.getValue();
//                ((ObjectNode) serie.getValue())._children.get("deaths")
                list.add(new CountryTimeSerie(LocalDate.parse(serie.getKey(), formatter), values.get("confirmed").asLong(), values.get("deaths").asLong(), values.get("recovered").asLong()));
            }
        }
        return list;
    }
}
