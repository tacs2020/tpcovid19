package edu.tacs.covid19.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@DynamicUpdate
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.LowerCaseStrategy.class)
public class Country implements Comparable<Country> {

    @Id
    private String countryRegion;

    @Embedded
    private CountryCode countryCode;
    private String provinceState;
    private LocalDateTime lastUpdate;

    @Embedded
    private Location location;

    private Long confirmed;
    private Long recovered;
    private Long deaths;
    private LocalDateTime firstCaseDate; // Implemented the offset country

    @JsonDeserialize(using = CountryTimeSerieDeserializer.class)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "country_region")
    private List<CountryTimeSerie> timeseries;

    public Country() {}

    public Country(String countryRegion, CountryCode countryCode, LocalDateTime lastUpdate, Location location, Long confirmed, Long recovered, Long deaths) {
        this.countryRegion = countryRegion;
        this.countryCode = countryCode;
        this.lastUpdate = lastUpdate;
        this.location = location;
        this.confirmed = confirmed;
        this.recovered = recovered;
        this.deaths = deaths;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    public String getProvinceState() {
        return provinceState;
    }
    public void setProvinceState(String provinceState) {
        this.provinceState = provinceState;
    }

    public String getCountryRegion() {
        return countryRegion;
    }
    public void setCountryRegion(String countryRegion) {
        this.countryRegion = countryRegion;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Location getLocation() {
        return location;
    }
    public void setLocation(Location location) {
        this.location = location;
    }


    public Long getConfirmed() {
        return confirmed;
    }
    public void setConfirmed(Long confirmed) {
        this.confirmed = confirmed;
    }

    public Long getRecovered() {
        return recovered;
    }
    public void setRecovered(Long recovered) {
        this.recovered = recovered;
    }

    public Long getDeaths() {
        return deaths;
    }
    public void setDeaths(Long deaths) {
        this.deaths = deaths;
    }

    public LocalDateTime getFirstCaseDate() {
        return firstCaseDate;
    }
    public void setFirstCaseDate(LocalDateTime firstCaseDate) {
        this.firstCaseDate = firstCaseDate;
    }

    public List<CountryTimeSerie> getTimeseries() {
        return timeseries;
    }
    public void setTimeseries(List<CountryTimeSerie> timeseries) {
        this.timeseries = timeseries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return countryRegion.equals(country.countryRegion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryRegion);
    }

    @Override
    public int compareTo(Country o) {
        return this.countryRegion.compareTo(o.countryRegion);
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryRegion='" + countryRegion + '\'' +
                ", provinceState='" + provinceState + '\'' +
                ", totalCases=" + confirmed +
                '}';
    }
}