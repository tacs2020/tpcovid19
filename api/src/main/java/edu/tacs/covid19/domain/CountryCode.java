package edu.tacs.covid19.domain;

import javax.persistence.Embeddable;

@Embeddable
public class CountryCode {

    private String iso3;
    private String iso2;

    public CountryCode() {}

    public CountryCode(String iso3, String iso2) {
        this.iso3 = iso3;
        this.iso2 = iso2;
    }

    public String getIso3() {
        return iso3;
    }
    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    public String getIso2() {
        return iso2;
    }
    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }

}