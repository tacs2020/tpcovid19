package edu.tacs.covid19.utils;


import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

public class CopyProps {

    static final Logger logger = LoggerFactory.getLogger(CopyProps.class);

    public static <T> T copyPropertiesWithReadOnlyProps(T source, T target, Set<String> readOnlyProps) {
        try {
            PropertyUtils.describe(source).entrySet().stream()
                    .filter(e -> e.getValue() != null && ! e.getKey().equals("class"))
                    .filter(e -> ! readOnlyProps.contains(e.getKey()))
                    .forEach(e -> {
                        try {
                            PropertyUtils.setProperty(target, e.getKey(), e.getValue());
                        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
                            logger.error("Error copying property: " + e.getKey(), ex);
                        }
                    });
        } catch (NoSuchMethodException | IllegalArgumentException | IllegalAccessException | InvocationTargetException ex) {}
        return target;
    }
}
