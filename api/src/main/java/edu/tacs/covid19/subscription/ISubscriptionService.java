package edu.tacs.covid19.subscription;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ISubscriptionService<T> {
    List<T> findAll();
    List<T> findAllByUserId(UUID userId);
    Optional<T> save(UUID userId, T subscription);
    Optional<T> findOneByUserId(UUID userId, String subscriptionId);
}
