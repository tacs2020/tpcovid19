package edu.tacs.covid19.subscription;

import edu.tacs.covid19.domain.Subscription;
import edu.tacs.covid19.domain.User;
import edu.tacs.covid19.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class SubscriptionService implements ISubscriptionService<Subscription> {
    final Logger logger = LoggerFactory.getLogger(SubscriptionService.class);

    @Autowired
    ISubscriptionRepository repository;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<Subscription> findAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Subscription> findAllByUserId(UUID id) {
        List<Subscription> list = repository.findByUser_Id(id);
        if (list.size() < 1) throw new ResourceNotFoundException("Subscription not found");
        return list;
    }

    @Override
    public Optional<Subscription> save(UUID userId, Subscription subscription) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
        subscription.setUser(user);
        return Optional.of(repository.save(subscription));
    }

    @Override
    public Optional<Subscription> findOneByUserId(UUID userId, String subscriptionId) {
        return Optional.empty();
    }


}
