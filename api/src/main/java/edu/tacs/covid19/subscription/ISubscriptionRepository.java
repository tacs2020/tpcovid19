package edu.tacs.covid19.subscription;

import edu.tacs.covid19.domain.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RestResource(exported = false)
public interface ISubscriptionRepository extends JpaRepository<Subscription, UUID> {
    List<Subscription> findByUser_Id(@Param("id") UUID id);
}