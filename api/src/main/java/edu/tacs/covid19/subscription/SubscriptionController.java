package edu.tacs.covid19.subscription;

import edu.tacs.covid19.domain.Subscription;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class SubscriptionController {

    ISubscriptionService service;

    public SubscriptionController(ISubscriptionService service) {
        this.service = service;
    }

    @GetMapping("/subscriptions")
    public List<Subscription> findAll() {
         return service.findAll();
    }

    @GetMapping("/users/{userId}/subscriptions")
    public List<Subscription> findAllByUserId(@PathVariable UUID userId) {
        return service.findAllByUserId(userId);
    }

    @PostMapping("/users/{userId}/subscriptions")
    public Optional<Subscription> saveUserSubscriptions(@PathVariable UUID userId, @RequestBody Subscription subscription) {
        return service.save(userId, subscription);
    }

    @GetMapping("/users/{userId}/subscriptions/{subscriptionId}")
    public Optional<Subscription> findOneByUserId(@PathVariable UUID userId, @PathVariable String subscriptionId) {
        return service.findOneByUserId(userId, subscriptionId);
    }

    /*@PatchMapping("/users/{userId}/subscriptions")
    public Optional<Subscription> saveUserSubscriptionsByIds(String userId) {
        try (Stream<Subscription> stream = service.findAllByUserId(userId)) {
            return stream.collect(Collectors.toList());
        }
    }*/
}
