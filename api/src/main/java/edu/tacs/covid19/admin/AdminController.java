package edu.tacs.covid19.admin;

import edu.tacs.covid19.country.ICountryService;
import edu.tacs.covid19.domain.Country;
import edu.tacs.covid19.domain.User;
import edu.tacs.covid19.user.IUserService;
import edu.tacs.covid19.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    ICountryService countryService;

    @Autowired
    IUserService userService;

    @Autowired
    UserRepository userRepository;

    @GetMapping
    public User userById(@PathVariable String username) {
        User userRetrieved = this.userRepository.findByUsername(username);
        if (userRetrieved == null){
            throw new ResourceNotFoundException("User not found");
        }
        return userRetrieved;
    }



    @GetMapping("/{iso3}")
    public Optional<Country> country(@PathVariable String iso3) throws InvocationTargetException, IllegalAccessException {
        return this.countryService.findById(iso3);
    }
}