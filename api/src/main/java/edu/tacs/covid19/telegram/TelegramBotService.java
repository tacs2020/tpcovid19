package edu.tacs.covid19.telegram;

import edu.tacs.covid19.country.ICountryService;
import edu.tacs.covid19.domain.Country;
import edu.tacs.covid19.domain.Subscription;
import edu.tacs.covid19.subscription.ISubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * To chat in telegram: t.me/covid2020botBot
 *
 */
@Service
public class TelegramBotService implements ITelegramBotService {

    @Autowired
    ISubscriptionService subscriptionService;

    @Autowired
    ICountryService countryService;

    @Override
    public String getSubscriptionsListAsString() {
        return subscriptionService.findAll().collect(Collectors.toList()).toString();
    }

    @Override
    public String getCountriesListAsString() {
        return countryService.findAllAsList().toString();
    }

    @Override
    public Map<String, String> getSubscriptionsAsMap() {
        List<Subscription> list = (List<Subscription>) subscriptionService.findAll().collect(Collectors.toList());
        Map<String, String> map = new HashMap<>();
        list.forEach(item -> map.put(item.getId().toString(), item.getName()));
        return map;
    }

    @Override
    public Map<String, String> getSCountriesAsMap() {
        List<Country> list = countryService.findAllAsList();
        Map<String, String> map = new HashMap<>();
        list.forEach(item -> map.put(item.getCountryRegion(), item.getCountryRegion()));
        return map;
    }

    @Override
    public String findSubscriptionById(String substring) {
        Subscription sub = (Subscription) subscriptionService.findOneByUserId("userId", substring).get();
        if (sub != null) {
            return sub.toString();
        } else {
            return null;
        }
    }

    @Override
    public String findCountryById(String data) {
        Country country = (Country) countryService.findById(data).get();
        if (country != null) {
            return country.toString();
        } else {
            return null;
        }
    }
}
