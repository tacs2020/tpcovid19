package edu.tacs.covid19.telegram;

import edu.tacs.covid19.country.ICountryService;
import edu.tacs.covid19.subscription.ISubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * To chat in telegram: t.me/covid2020botBot
 *
 */
public interface ITelegramBotService {
    public String getSubscriptionsListAsString();
    public String getCountriesListAsString();
    Map<String, String> getSubscriptionsAsMap();

    Map<String, String> getSCountriesAsMap();

    String findSubscriptionById(String substring);

    String findCountryById(String data);
}
