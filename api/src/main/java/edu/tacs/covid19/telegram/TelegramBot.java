package edu.tacs.covid19.telegram;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;
import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * To chat in telegram: t.me/covid2020botBot
 *
 */
@Service
public class TelegramBot extends TelegramLongPollingBot {

    public static final String TOKEN = "993251534:AAEDHY25KnHFkkf2HqIHCLjLFMp1ujixHYg";

    final
    ITelegramBotService telegramBotService;

    public TelegramBot(ITelegramBotService telegramBotService) {
        this.telegramBotService = telegramBotService;
    }

    @PostConstruct
    public void registerBot(){
        TelegramBotsApi botsApi = new TelegramBotsApi();
        try {
            botsApi.registerBot(this);
            System.out.println("TelegramBotService.afterPropertiesSet:registerBot finish");
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        System.out.println("Received single update! " + update );
    }

    @Override
    public void onUpdatesReceived(List<Update> updates) {
        for (Update update : updates) {
            if (!update.hasCallbackQuery()) {
                // build response
                buildResponseFromSimpleText(update);
            } else {
                //handle inline response
                handleCallBack(update);
            }
        }
    }

    private void handleCallBack(Update update) {
        String text = null;
        ReplyKeyboardMarkup keyboard = null;
        InlineKeyboardMarkup markupInline = null;
        Long chatId;
        String data = getIdFromData(update.getCallbackQuery().getData());
        String type = getTypeFromData(update.getCallbackQuery().getData());

        switch (type) {
            case "subscription":
                text = telegramBotService.findSubscriptionById(data);
                break;
            case "country":
                text = telegramBotService.findCountryById(data);
                break;
            default:
                text = update.getCallbackQuery().getData();
                break;
        }

        chatId = update.getCallbackQuery().getMessage().getChatId();

        try {
            sendMessage(chatId, text, keyboard, markupInline);
        } catch (InvalidObjectException e) {
            e.printStackTrace();
        }
    }

    private String getTypeFromData(String data) {
        return data.substring(0,data.indexOf("-"));
    }

    private String getIdFromData(String data) {
        return data.substring(data.indexOf("-") + 1,data.length());
    }

    private void buildResponseFromSimpleText(Update update) {
        // build response keyboard
        ReplyKeyboardMarkup keyboard = buildReplyKeyboard(update.getMessage());
        String text = update.getMessage().getText();
        String response;
        InlineKeyboardMarkup markupInline = null;

        switch (text) {
            case "getSubscriptions":
                response = telegramBotService.getSubscriptionsListAsString();
                markupInline = buildInlineMarkup(telegramBotService.getSubscriptionsAsMap(), "subscription-");
                break;
            case "getCountries":
                response = telegramBotService.getCountriesListAsString();
                markupInline = buildInlineMarkup(telegramBotService.getSCountriesAsMap(), "country-");
                break;

            default:
                response = "Choose from options";
                break;
        }
        Long chatId = update.getMessage().getChatId();
        try {
            sendMessage(chatId, response, keyboard, markupInline);
        } catch (InvalidObjectException e) {
            e.printStackTrace();
        }

    }

    private InlineKeyboardMarkup buildInlineMarkup(Map<String, String> subscriptionsIdsListAsString, String prefix) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();
        subscriptionsIdsListAsString.forEach((key, value) -> {
            rowInline.add(new InlineKeyboardButton().setText(value).setCallbackData(prefix + key));
        });
        // Set the keyboard to the markup
        rowsInline.add(rowInline);
        // Add it to the message
        markupInline.setKeyboard(rowsInline);
        return markupInline;
    }

    @Override
    public String getBotUsername() {
        return "covid2020botBot";
    }

    @Override
    public String getBotToken() {
        return TOKEN;
    }

    private void sendMessage(Long chatId, String text, ReplyKeyboardMarkup keyboard, InlineKeyboardMarkup markupInline) throws InvalidObjectException {
        SendMessage sendMessageRequest = new SendMessage();

        sendMessageRequest.setText(text);
        sendMessageRequest.setChatId(chatId);
        if (markupInline != null) {
            sendMessageRequest.setReplyMarkup(markupInline);
        } else {
            sendMessageRequest.setReplyMarkup(keyboard);
        }

        try {
            execute(sendMessageRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    public synchronized ReplyKeyboardMarkup buildReplyKeyboard(Message m) {

        // Create a keyboard
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        // Create a list of keyboard rows
        List<KeyboardRow> keyboard = new ArrayList<>();
        // First keyboard row
        KeyboardRow keyboardFirstRow = new KeyboardRow();

        // Add buttons to DEFAULT ROW
        keyboardFirstRow.add(new KeyboardButton("getSubscriptions"));
        keyboardFirstRow.add(new KeyboardButton("getCountries"));

        // Second keyboard row
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(new KeyboardButton("getTableForList"));
        keyboardSecondRow.add(new KeyboardButton("addCountry"));
        keyboardSecondRow.add(new KeyboardButton("getCountriesFromList"));

        // Add all of the keyboard rows to the list
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        // and assign this list to our keyboard
        replyKeyboardMarkup.setKeyboard(keyboard);

        return replyKeyboardMarkup;
    }
}
