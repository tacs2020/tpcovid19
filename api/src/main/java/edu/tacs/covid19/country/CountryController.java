package edu.tacs.covid19.country;

import edu.tacs.covid19.domain.Country;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/countries")
public class CountryController {

    ICountryService service;

    public CountryController(ICountryService service) {
        this.service = service;
    }

    @GetMapping
    public List<Country> countries() {
        return this.service.findAll();
    }

    @GetMapping("/{iso3}")
    public Optional<Country> country(@PathVariable String iso3) throws InvocationTargetException, IllegalAccessException {
        return service.findById(iso3);
    }
}