package edu.tacs.covid19.country;

import edu.tacs.covid19.domain.Country;
import edu.tacs.covid19.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

//@Primary
//@Service
public class CountryImplService implements ICountryService{

    @Autowired
    private CountryRepository repository;

    @Autowired
    private BackendService backendService;

    final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Override
    public List<Country> findAll() {return this.repository.findAll();}

    @Override
    public Optional findById(Object o) {
        return Optional.empty();
    }

}
