package edu.tacs.covid19.country;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class BackendService {
    @Autowired
    private ObjectMapper objectMapper;

    public String get(String endPoint) throws Exception {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(endPoint);
        CloseableHttpResponse response = httpClient.execute(request);
        if (response.getStatusLine().getStatusCode() == 200) {
            HttpEntity entity = response.getEntity();
            String body = EntityUtils.toString(entity);
            return body;
        }
        else{
            throw new Exception("Error GET request. Status Code: " + response.getStatusLine().getStatusCode());
        }
    }

    public <T> List<T> getListByKey(String endpoint, String key, Class<T[]> aClass) throws Exception {
        String response = this.get(endpoint);
        JSONObject jsonObject = new JSONObject(response);
        JSONArray resultsJson = jsonObject.getJSONArray(key);
        List<T> resultObject = Arrays.asList(objectMapper.readValue(resultsJson.toString(), aClass));
        return resultObject;
    }

    public  <T> List<T> getList(String endpoint, Class<T> classType) throws Exception {
        String response =  this.get(endpoint);
        JSONArray miLista = new JSONArray(response);
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, classType);
        List<T> ret = objectMapper.readValue(miLista.toString(), javaType);
        return ret;
    }
}
