package edu.tacs.covid19.country;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

public interface ICountryService<T, ID> {
    List<T> findAll();
    Optional<T> findById(ID id) throws InvocationTargetException, IllegalAccessException;
}
