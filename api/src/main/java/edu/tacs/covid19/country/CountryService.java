package edu.tacs.covid19.country;

import edu.tacs.covid19.domain.Country;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.*;

@Service
public class CountryService implements ICountryService<Country, String> {
    final Logger logger = LoggerFactory.getLogger(CountryService.class);

    @Autowired
    CountryServiceClient countryClient;

    @Autowired
    CountryRepository repository;


    @Override
    public List<Country> findAll() {
        return countryClient.getLatestAll();
    }

    @Override
    public Optional<Country> findById(String id) throws InvocationTargetException, IllegalAccessException {
        // if exist in DB get the las one if not exist get all the serie from the service
        Country countryInDB = repository.findById(id).orElse(countryClient.getTimeSeriesByIso3(id));

        Country country;
        // Update the with the latest
        if (countryInDB.getLastUpdate().toLocalDate().isBefore(LocalDate.now())) {
            country = countryClient.getLatestByIso3(id);
            BeanUtils.copyProperties(countryInDB, country);

        } else
            country = countryInDB;

        repository.save(country);

        return Optional.of(country);
    }

}