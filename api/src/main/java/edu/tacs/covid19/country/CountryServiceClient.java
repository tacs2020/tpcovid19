package edu.tacs.covid19.country;

import edu.tacs.covid19.domain.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class CountryServiceClient {
    final Logger logger = LoggerFactory.getLogger(CountryServiceClient.class);

    private static final String URI_BASE = "https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/";
    private static final String LATEST = "latest";
    private static final String TIME_SERIES = "timeseries";


    private final RestTemplate restTemplate;

    public CountryServiceClient(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Arrays.asList(MediaType.TEXT_HTML, MediaType.APPLICATION_JSON));
        this.restTemplate.getMessageConverters().add(0, converter);
    }

    /**
     * GET from latest
     */
    public List<Country> getLatestAll() {
        ResponseEntity<Country[]> response = this.restTemplate.getForEntity(URI_BASE+LATEST+"?onlyCountries=true", Country[].class);
        return Arrays.asList(response.getBody());
    }

    public Country getLatestByIso3(String iso3) {
        ResponseEntity<Country[]> response = this.restTemplate.getForEntity(URI_BASE+LATEST+"?iso3="+iso3, Country[].class);
        Country[] body = response.getBody();
        if (body.length == 1)
            return body[0];
        else
            throw new ResourceNotFoundException("The country "+iso3+" not Found");
    }


    /**
     * Get From TimeSeries
     */
    public Country getTimeSeriesByIso3(String iso3) {
        ResponseEntity<Country[]> response = this.restTemplate.getForEntity(URI_BASE+TIME_SERIES+"?iso3="+iso3, Country[].class);
        Country[] body = response.getBody();
        if (body.length == 1)
            return body[0];
        else
            throw new ResourceNotFoundException("The country "+iso3+" not Found");
    }
}
