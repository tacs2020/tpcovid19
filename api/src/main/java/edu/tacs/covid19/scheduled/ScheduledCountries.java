package edu.tacs.covid19.scheduled;

import edu.tacs.covid19.country.BackendService;
import edu.tacs.covid19.country.CountryImplService;
import edu.tacs.covid19.country.CountryRepository;
import edu.tacs.covid19.country.CountryService;
import edu.tacs.covid19.domain.Country;
import edu.tacs.covid19.domain.CountryLatest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

@Component
public class ScheduledCountries {

	final Logger logger = LoggerFactory.getLogger(CountryService.class);

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private BackendService backendService;

	private final String URL_LATEST = "https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/latest";
	private final String URL_TIMESERIES = "https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/timeseries";

	public void getCountries() throws Exception {
		try {
			List<Country> countries = this.backendService.getList(URL_LATEST, Country.class);
			List<Country> countriesSaved = this.countryRepository.saveAll(new HashSet<>(countries));
			logger.info("Info: Se recuperaron {} paises de la base de datos", countriesSaved.size());
		}catch (UnknownHostException e){
			final Integer timeout = 1;
			logger.error("Error: No se recuperadon (paises:provincestate) de la api externa, se vuelve a intentar cada {} segundos.", timeout);
			TimeUnit.SECONDS.sleep(timeout);
			this.getCountries();
		}

	}

	public void recuperarPaisesApiExterna() throws Exception{
		final String url = "https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/latest";
		List<Country> countries = this.backendService.getList(url, Country.class);
		logger.info("Info: Paises recuperadon {} (paises:provincestate) de la api externa", countries.size());

		List<Country> countriesSaved = this.countryRepository.saveAll(new HashSet<>(countries));
		logger.info("Info: Se guardaron {} paises en la base de datos.", countriesSaved.size());

		List<Country> countriesRetrieved = this.countryRepository.findAll();
		logger.info("Info: Se recuperaron {} paises de la base de datos", countriesSaved.size());

	}
}
