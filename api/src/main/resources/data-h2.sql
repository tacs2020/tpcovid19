insert into user(id, username) values ('f2c8365e2f7446419587a710a2b50dbd', 'tadeo');
insert into user(id, username) values ('0ade8385caa34c7b9a7bce1c50f7b67e', 'roberto');
insert into user(id, username) values ('1e3c5de5a394417787c3a253e78193bd', 'daniel');

insert into SUBSCRIPTION (ID, NAME, USER_ID) values ('fc0e0627aadf4276b13d7dbdc6222c06', 'Latinamerica', 'f2c8365e2f7446419587a710a2b50dbd');
insert into SUBSCRIPTION (ID, NAME, USER_ID) values ('d1497b025373448590bbfdf30c0ea8e1', 'America', 'f2c8365e2f7446419587a710a2b50dbd');

insert into COUNTRY(COUNTRY_REGION, ISO3, ISO2, LAT, LNG) values ('Argentina','ARG','AR',-38.4161,-63.6167);
insert into SUBSCRIPTION_COUNTRIES (SUBSCRIPTION_ID, COUNTRIES_COUNTRY_REGION) VALUES ('fc0e0627aadf4276b13d7dbdc6222c06', 'Argentina');

insert into COUNTRY(COUNTRY_REGION, ISO3, ISO2, LAT, LNG) values ('US','USA','US',-38.4161,-63.6167);
insert into SUBSCRIPTION_COUNTRIES (SUBSCRIPTION_ID, COUNTRIES_COUNTRY_REGION) VALUES ('d1497b025373448590bbfdf30c0ea8e1', 'Argentina');
insert into SUBSCRIPTION_COUNTRIES (SUBSCRIPTION_ID, COUNTRIES_COUNTRY_REGION) VALUES ('d1497b025373448590bbfdf30c0ea8e1', 'US');
