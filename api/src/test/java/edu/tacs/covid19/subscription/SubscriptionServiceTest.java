package edu.tacs.covid19.subscription;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit Test for SubscriptionService
 */
class SubscriptionServiceTest {

    private ISubscriptionService service = new SubscriptionService();

    @BeforeEach
    void setUp() {
    }

    @Test
    void findAll() {
        assertEquals(2, service.findAll().size());
    }
}