package edu.tacs.covid19.subscription;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration Test for SubscriptionController
 */
@AutoConfigureMockMvc
@ContextConfiguration(classes = {SubscriptionController.class, SubscriptionService.class})
@WebMvcTest
class SubscriptionControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("findAll Subscriptions \uD83D\uDE03")
    void finAll() throws Exception {
        MvcResult resultRest = mockMvc.perform(MockMvcRequestBuilders.get("/subscriptions")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String result = resultRest.getResponse().getContentAsString();
        assertNotNull(result);
//        System.out.println(result);
//        assertEquals(dow, resultDOW);
    }
}