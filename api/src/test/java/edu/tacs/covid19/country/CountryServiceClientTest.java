package edu.tacs.covid19.country;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.tacs.covid19.domain.Country;
import edu.tacs.covid19.domain.CountryCode;
import edu.tacs.covid19.domain.Location;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

/**
 * Unit Test for CountryServiceClient
 */
@RestClientTest(CountryServiceClient.class)
class CountryServiceClientTest {

    @Autowired
    private CountryServiceClient client;

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;


    @BeforeEach
    public void setUp() throws Exception {
        String countryString = objectMapper.writeValueAsString(new Country("Argentina", new CountryCode("ARG","AR"),
                LocalDateTime.parse("2020-05-12T03:42:00.002"), new Location(-38.4161, -63.6167), 6278L, 1837L, 314L));

        this.server.expect(requestTo("https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/latest?iso3=ARG"))
                .andRespond(withSuccess(countryString, MediaType.APPLICATION_JSON));
    }

    @Test
    void getLatestByIso3_ARG() throws Exception{
        Country country = this.client.getLatestByIso3("ARG");

        assertThat(country.getCountryRegion()).isEqualTo("Argentina");
    }
}